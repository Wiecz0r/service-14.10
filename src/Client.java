import java.util.ArrayList;
import java.util.List;

public class Client implements Visitable{
	private String number;
	private List<Order>orders;
	
	public Client(String number){
		this.number=number;
		this.orders=new ArrayList<>();
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public void addOrder(Order order){
		orders.add(order);
	}
	@Override
	public void accept(Visitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
	@Override
	public String giveReport() {
		// TODO Auto-generated method stub
		return number + orders;
	}
}