import java.util.ArrayList;
import java.util.List;

public class ClientGroup implements Visitable{
	private String memberName;
	private List<Client>Clients;
	
	public ClientGroup(String memberName){
		Clients=new ArrayList<>();
		this.memberName=memberName;
	}
	public void addClient(Client client){
		Clients.add(client);
	}
	@Override
	public void accept(Visitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
	@Override
	public String giveReport() {
		// TODO Auto-generated method stub
		return memberName;
	}
}