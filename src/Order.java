import java.util.Date;
import java.util.ArrayList;
import java.util.List;

public class Order implements Visitable{
	private String number;
	private Date orderDate;
	private double orderTotalPrice;
	private List<Product>productList;
	
	public Order(String number){
		this.number=number;
		this.orderDate=new Date();
		this.productList=new ArrayList<>();
	}
	
	public String getNumber() {
		return number;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public double getOrderTotalPrice() {
		return orderTotalPrice;
	}
	public void addProduct(Product product){
		productList.add(product);
	}
	@Override
	public void accept(Visitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
	@Override
	public String giveReport() {
		// TODO Auto-generated method stub
		return number + orderDate;
	}
}